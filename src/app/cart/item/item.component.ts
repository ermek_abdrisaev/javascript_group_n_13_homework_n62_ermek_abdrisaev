import { Component, Input, OnInit } from '@angular/core';
import { CartItem } from '../../shared/cart-item.model';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent{
  @Input() cartItem!: CartItem;


}
