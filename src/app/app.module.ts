import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NewGameComponent } from './new-game/new-game.component';
import { GamesComponent } from './games/games.component';
import { GameItemComponent } from './games/game-item/game-item.component';
import { CartComponent } from './cart/cart.component';
import { ItemComponent } from './cart/item/item.component';
import { FormsModule } from '@angular/forms';
import { GameService } from './shared/game.service';
import { CartService } from './shared/cart.service';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found.component';
import { ManageGamesComponent } from './games/manage-games/manage-games.component';
import { GamesDetailsComponent } from './games/manage-games/games-details/games-details.component';
import { AppRoutingModule } from './app-routing.module';
import { PlatformComponent } from './platform/platform.component';
import { PlatformItemComponent } from './platform/platform-item/platform-item.component';



@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    NewGameComponent,
    GamesComponent,
    GameItemComponent,
    CartComponent,
    ItemComponent,
    HomeComponent,
    NotFoundComponent,
    ManageGamesComponent,
    GamesDetailsComponent,
    PlatformComponent,
    PlatformItemComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [GameService, CartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
