import { Component } from '@angular/core';
import { Game } from './shared/game.model';
import { CartItem } from './shared/cart-item.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  location = 'home';

  onNavigate(where: string){
    this.location = where;
  }

}
