import { EventEmitter } from '@angular/core';
import { CartItem } from './cart-item.model';
import { Game } from './game.model';

export class CartService {
  cartItemsChange = new EventEmitter<CartItem[]>();
  private cartItems: CartItem[] = [];

  getItems(){
    return this.cartItems.slice();
  }

  addGameToCart(game: Game){
    const existingItem = this.cartItems.find(cartItem => cartItem.game === game);

    if(existingItem){
      existingItem.amount++
    } else {
      const newItem = new CartItem(game);
      this.cartItems.push(newItem);
    }
    this.cartItemsChange.emit(this.cartItems);
  }
}
