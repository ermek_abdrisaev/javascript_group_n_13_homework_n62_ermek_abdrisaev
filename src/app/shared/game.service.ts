import { Game } from './game.model';
import { EventEmitter } from '@angular/core';

export class GameService {
  gamesChange = new EventEmitter<Game[]>();

  private games: Game[] = [
    new Game('Super Sonic', 'https://play-lh.googleusercontent.com/oOaQ1mvLXPbjWAwrVna1S6Yn-e0NnDwueh5RtQTWvYqljlnF_VbaSBuWiktbwKTgOA', 'Sonic the Hedgehog is a game made by Sega and the main character is Sonic where he has supersonic speed reaching the speed of sound', 'Sega Genesis'),
    new Game('Alien Soldier', 'https://static1.srcdn.com/wordpress/wp-content/uploads/2021/04/Older-Games-With-Great-Graphics-Alien-Soldier.jpg?q=50&fit=crop&w=740&h=370&dpr=1.5', 'Is a side-scrolling run and gun video game in which the player controls the main character, Epsilon-Eagle, through 25 stages and 26 bosses', 'Sega Genesis'),
    new Game('StarTropics', 'https://assets3.thrillist.com/v1/image/1744874/1584x1054/crop;webp=auto;jpeg_quality=60.jpg', 'Is an action-adventure video game released by Nintendo in 1990 for the NES', 'NES'),
  ];

  getGames(){
    return this.games.slice();
  }

  getGame(index: number){
    return this.games[index];
  }

  getGamesByPlatform(platform: string){
    const platGames: Game[] = [];
    this.games.forEach(game => {
      if (game.platform === platform) {
        platGames.push(game);
      }
    });
    return platGames;
  }

  addGame(game: Game){
    this.games.push(game);
    this.gamesChange.emit(this.games);
  }
}
