import { Game } from './game.model';

export class CartItem {
  constructor(
    public game: Game,
    public amount: number = 1,
  ){}
}
