import { Component, Input, OnInit } from '@angular/core';
import { GameService } from '../shared/game.service';
import { Game } from '../shared/game.model';

@Component({
  selector: 'app-platform',
  templateUrl: './platform.component.html',
  styleUrls: ['./platform.component.css']
})
export class PlatformComponent implements OnInit {
  games: Game[] = [];

  constructor(private gameService: GameService){}

  ngOnInit(): void {
    this.games = this.gameService.getGames();
    this.gameService.gamesChange.subscribe((games : Game[]) =>{
      this.games = games;
    });

  }



}
