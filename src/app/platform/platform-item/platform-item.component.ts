import { Component, Input, OnInit } from '@angular/core';
import { Game } from '../../shared/game.model';
import { GameService } from '../../shared/game.service';

@Component({
  selector: 'app-platform-item',
  templateUrl: './platform-item.component.html',
  styleUrls: ['./platform-item.component.css']
})
export class PlatformItemComponent implements OnInit {
  @Input() game!: Game;

  constructor(private  gameService: GameService) { }

  ngOnInit(): void {
  }

  onClick(){
    this.gameService.getGamesByPlatform;
  }

}
