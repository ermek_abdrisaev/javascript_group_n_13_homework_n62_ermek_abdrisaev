import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ManageGamesComponent } from './games/manage-games/manage-games.component';
import { EmptyGameComponent } from './games/manage-games/empty-game.component';
import { NewGameComponent } from './new-game/new-game.component';
import { GamesDetailsComponent } from './games/manage-games/games-details/games-details.component';
import { NotFoundComponent } from './not-found.component';
import { PlatformItemComponent } from './platform/platform-item/platform-item.component';
import { PlatformComponent } from './platform/platform.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {
    path: 'games', component: ManageGamesComponent, children: [
      {path: '', component: EmptyGameComponent},
      {path: 'new', component: NewGameComponent},
      {path: ':id', component: GamesDetailsComponent},

    ]
  },
  {path: 'platform', component: PlatformItemComponent},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
