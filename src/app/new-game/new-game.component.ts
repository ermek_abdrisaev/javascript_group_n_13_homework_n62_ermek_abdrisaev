import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Game } from '../shared/game.model';
import { GameService } from '../shared/game.service';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.css']
})
export class NewGameComponent implements OnInit {

  name = '';
  description = '';
  imageUrl = '';
  platform = 'select';

  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('imageInput') imageInput!: ElementRef;
  @ViewChild('descriptionInput') descriptionInput!: ElementRef;
  @ViewChild('platformInput') platformInput!: ElementRef;

  constructor(public gameService: GameService) {}

  ngOnInit(): void {
  }

  createGame(){
    const name: string = this.nameInput.nativeElement.value;
    const imageUrl: string = this.imageInput.nativeElement.value;
    const description: string = this.descriptionInput.nativeElement.value;
    const platform: string = this.platformInput.nativeElement.value;

    const game = new Game(name, imageUrl, description, platform);
    this.gameService.addGame(game);
  }

}
