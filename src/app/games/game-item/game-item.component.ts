import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Game } from '../../shared/game.model';
import { CartService } from '../../shared/cart.service';

@Component({
  selector: 'app-game-item',
  templateUrl: './game-item.component.html',
  styleUrls: ['./game-item.component.css']
})
export class GameItemComponent {
  @Input() game!: Game;

  constructor(private cartService: CartService){}

  onClick(){
    this.cartService.addGameToCart(this.game);
  }


}
