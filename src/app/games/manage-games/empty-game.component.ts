import { Component } from '@angular/core';

@Component({
  selector: 'app-emty-game',
  template: `
    <h4>Game details</h4>
    <p>No games is selected</p>
  `
})
export class EmptyGameComponent {}
