import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { GameService } from '../../../shared/game.service';
import { Game } from '../../../shared/game.model';

@Component({
  selector: 'app-games-details',
  templateUrl: './games-details.component.html',
  styleUrls: ['./games-details.component.css']
})
export class GamesDetailsComponent implements OnInit {
  game!: Game;
  constructor(private route: ActivatedRoute, private gameService: GameService) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) =>{
      const gameId = parseInt(params['id']);
      this.game = this.gameService.getGame(gameId);
    });
  }
}
