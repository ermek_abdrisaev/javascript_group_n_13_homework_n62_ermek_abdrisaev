import { Component, OnInit } from '@angular/core';
import { GameService } from '../../shared/game.service';
import { Game } from '../../shared/game.model';

@Component({
  selector: 'app-manage-games',
  templateUrl: './manage-games.component.html',
  styleUrls: ['./manage-games.component.css']
})
export class ManageGamesComponent implements OnInit {
  games!: Game[];
  constructor(private gameService: GameService) {}

  ngOnInit(): void {
    this.games = this.gameService.getGames();
    this.gameService.gamesChange.subscribe((games: Game[]) =>{
      this.games = games;
    });
  }



}
